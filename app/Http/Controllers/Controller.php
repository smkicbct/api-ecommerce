<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /*
    * Function to upload a file.
    *
    * @param $file is for files response and $folder for upload to selected folder.
    */
    public function doUpload($file,$folder)
    {
        $name = time().'.'.$file->getClientOriginalExtension(); // Time is default name
        $path = public_path($folder);
        if(!is_dir($path)){
            File::MakeDirectory($path,0777);
        }
        $file->move($path,$name);
        return $name;
    }

}
