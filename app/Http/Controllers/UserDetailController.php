<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\UserDetail;
use App\User;

class UserDetailController extends Controller
{
    /**
     * Show the User Detail.
     *
     * @param $request Request
     */
     public function show(Request $request)
     {
        $data = UserDetail::where('id_user',$request->id_user)->with('user')->get();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['userDetail'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['userDetail'] = 'Failed to find User';
            return $res;
        }
     }

    /**
     * Store the User Detail.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $id = $request->input('id_user');
        $sex = $request->input('sex');
        $dob = $request->input('dob');
        $phone = $request->input('phone');
        
        $data = UserDetail::create([
            'id_user' => $id,
            'sex' => $sex,
            'dob' => $dob,
            'phone' => $phone
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update user detail!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update user detail!';
            return response($res);
        }
    }

    /**
     * Update the User Detail.
     *
     * @param $request Request
     */
    public function update(Request $request)
    {
        $sex = $request->input('sex');
        $dob = $request->input('dob');
        $phone = $request->input('phone');

        $data = UserDetail::where('id_user',$request->id_user)->update([
            'sex' => $sex,
            'dob' => $dob,
            'phone' => $phone
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update user detail!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update user detail!';
            return response($res);
        }
    }

    /**
     * Check if User Detail is exist.
     *
     * @param $request Request
     */
     public function checkUser(Request $request)
     {
         $data = UserDetail::where('id_user', $request->input('id_user'))->get();
         if(count($data) > 0) {
            $res['success'] = true;
            $res['userDetail'] = $data;
            return $res;
         } else {
            $res['success'] = false;
            $res['userDetail'] = 'User data not found!';
            return $res;
         }
     }

     /**
     * Action if User Detail/Profile not exist.
     *
     * @param $request Request
     */
     public function action(Request $request)
     {
         $data = $this->checkUser($request);
         if ($data['success'] == true) {
            return $this->update($request);
         } else {
            return $this->store($request);
         }
     }
}
