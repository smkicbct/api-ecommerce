<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Register new user.
     *
     * @oaram $request Request
     */
    public function register(Request $request)
    {
        $hasher = app()->make('hash');
        $name = $request->input('name');
        $username = $request->input('username');
        $email = $request->input('email');
        $password = $hasher->make($request->input('password'));
        $roles = $request->input('id_roles');

        $register = User::create([
            'name' => $name,
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'id_roles' => $roles
        ]);

        if($register) {
            $res['success'] = true;
            $res['message'] = 'Success register!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to register!';
            return response($res);
        }
    }

    public function getUser(Request $request,$id)
    {
        $user = User::where('id_user', $id);
        if($user) {
            $res['success'] = true;
            $res['message'] = $user->with('userRole')->get();
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'fail';
            return response($res);
        }
    }
    
}
