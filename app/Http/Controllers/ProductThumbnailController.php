<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ProductThumbnail as PT;

class ProductThumbnailController extends Controller
{
    /**
     * Show the Product Thumbnail.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = PT::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['thumbnails'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['thumbnails'] = 'Failed to find thumbnails!';
            return $res;
        }
     }
    
    /**
     * Show the Product Thumbnail.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = PT::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['thumbnails'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['thumbnails'] = 'Failed to find thumbnails!';
            return $res;
        }
     }

    /**
     * Store the Product Thumbnail.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $name = $this->doUpload($request->file('photo'));
        $product = $request->input('id_product');
        
        $data = PC::create([
            'photo' => $name,
            'id_product' => $product
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update thumbnails!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update thumbnails!';
            return response($res);
        }
    }

    /**
     * Update the Product Thumbnail.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $name = $this->doUpload($request->file('photo'));
        $product = $request->input('id_product');
        
        $data = PC::findOrFail($id)->update([
            'photo' => $name,
            'id_product' => $product
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update category thumbnails!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update category thumbnails!';
            return response($res);
        }
    }

}
