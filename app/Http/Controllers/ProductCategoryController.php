<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ProductCategory as PC;

class ProductCategoryController extends Controller
{
    /**
     * Show the Product Categories.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = PC::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['categories'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['categories'] = 'Failed to find categories!';
            return $res;
        }
     }
    
    /**
     * Show the Product Categories.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = PC::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['categories'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['categories'] = 'Failed to find categories!';
            return $res;
        }
     }

    /**
     * Store the Product Categories.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $parent = $request->input('id_parent');
        
        $data = PC::create([
            'name' => $name,
            'id_parent' => $parent
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update category product!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update category product!';
            return response($res);
        }
    }

    /**
     * Update the Product Categories.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $name = $request->input('name');
        $parent = $request->input('id_parent');
        
        $data = PC::findOrFail($id)->update([
            'name' => $name,
            'id_parent' => $parent
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update category product!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update category product!';
            return response($res);
        }
    }

}
