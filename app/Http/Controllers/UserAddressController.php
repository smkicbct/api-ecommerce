<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\UserAddress as UA;

class UserAddressController extends Controller
{
    /**
     * Show the Address.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = UA::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['address'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['address'] = 'Failed to find address!';
            return $res;
        }
     }
    
    /**
     * Show the Address.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = UA::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['address'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['address'] = 'Failed to find address!';
            return $res;
        }
     }

    /**
     * Store the Address.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $id = $request->input('id_user');
        $name_address = $request->input('name_address');
        $on_behalf = $request->input('on_behalf');
        $province = 32; // id_province 32 from db(Jawa Barat)
        $regency = 3273; // id_regency 3273 from db(Kota Bandung)
        $disctrict = $request->input('disctrict');
        $village = $request->input('village');
        $address = $request->input('address');
        $postal_code = $request->input('postal_code');
        
        $data = UA::create([
            'id_user' => $id,
            'name_Address' => $name_address,
            'on_behalf' => $on_behalf,
            'id_province' => $province,
            'id_regency' => $regency,
            'id_district' => $disctrict,
            'id_village' => $village,
            'address' => $address,
            'postal_code' => $postal_code
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success insert address!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to insert address!';
            return response($res);
        }
    }

    /**
     * Update the Address.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $name = $request->input('name');
        $id = $request->input('id_user');
        $name_address = $request->input('name_address');
        $on_behalf = $request->input('on_behalf');
        $province = 32; // id_province 32 from db(Jawa Barat)
        $regency = 3273; // id_regency 3273 from db(Kota Bandung)
        $disctrict = $request->input('kecamatan');
        $village = $request->input('kelurahan');
        $address = $request->input('address');
        $postal_code = $request->input('postal_code');
        
        $data = UA::findOrFail($id)->update([
            'name' => $name,
            'id_user' => $id,
            'name_Address' => $name_address,
            'on_behalf' => $on_behalf,
            'id_province' => $province,
            'id_regency' => $regency,
            'id_district' => $disctrict,
            'id_village' => $village,
            'address' => $address,
            'postal_code' => $postal_code,
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update address!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update address!';
            return response($res);
        }
    }
}
