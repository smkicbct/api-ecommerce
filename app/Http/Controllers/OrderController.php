<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Order;

class ProductController extends Controller
{
    /**
     * Show the Orders.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = Order::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['orders'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['orders'] = 'Failed to find Orders!';
            return $res;
        }
     }
    
    /**
     * Show the Order.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = Order::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['order'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['order'] = 'Failed to find Order!';
            return $res;
        }
     }

    /**
     * Store the Product.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $buyer = $request->input('id_buyer');
        $address = $request->input('id_address');
        $product = $request->input('id_product');
        $quantity = $request->input('quantity');
        $price = $request->input('price');
        $note = $request->input('note');
        $payment = $request->input('payment');
        $status = $request->input('status');
        
        $data = Order::create([
            'id_buyer' => $buyer,
            'id_address' => $address,
            'id_product' => $product,
            'quantity' => $quantity,
            'price' => $price,
            'note' => $note,
            'payment' => $payment,
            'status' => $status
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update order!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update order!';
            return response($res);
        }
    }

    /**
     * Update the Product.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $buyer = $request->input('id_buyer');
        $address = $request->input('id_address');
        $product = $request->input('id_product');
        $quantity = $request->input('quantity');
        $price = $request->input('price');
        $note = $request->input('note');
        $payment = $request->input('payment');
        $status = $request->input('status');
        
        $data = Order::findOrFail($id)->update([
            'id_buyer' => $buyer,
            'id_address' => $address,
            'id_product' => $product,
            'quantity' => $quantity,
            'price' => $price,
            'note' => $note,
            'payment' => $payment,
            'status' => $status
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update order!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update order!';
            return response($res);
        }
    }

    public function when(Request $request,$id)
    {
        if($request->keyword) {
            $data = Order::where('id_user',$id)->when($request->status, function ($query) use ($request) {
                $query->where('status', 'like', "%$request->keyword%");
            })->get();
            return $data;
        } else {
            return $this->index()->where('id_user',$id);
        }
    }

}
