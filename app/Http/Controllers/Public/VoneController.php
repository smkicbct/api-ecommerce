<?php

namespace App\Http\Controllers;

class VoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /*
    * Get All Categories
    *
    */
    public function getAllCat() {
        $class_helper = new App\ProductCategoryController();
        return $class_helper->index();
    }

    /*
    * Get Per Categories
    *
    */
    public function getPerCat($id) {
        $class_helper = new App\ProductCategoryController();
        return $class_helper->show($id);
    }
    
    /*
    * Get User Detail
    *
    */
    public function getUserDetail() {
        $class_helper = new App\UserDetailController();
        return $class_helper->show($id);
    }

    /*
    * Update User Detail
    *
    */
    public function actionUserDetail(Request $request) {
        $class_helper = new App\UserDetailController();
        return $class_helper->action($request);
    }


    /*
    * Get User Address
    *
    */
    // public function getUserAddress(Request $request) {
    //     $class_helper = new App\UserAddressController();
    //     return $class_helper->index($request);
    // } so Stuck

    /*
    * Update User Detail
    *
    */
    public function actionUserAddress(Request $request) {
        $class_helper = new App\UserAddressController();
        // $where = $class_helper->index()->where('id_address', $request->reknya)
        //                             ->orWhere('id',$request->orRek);
    }

    /*
    * Get Order List
    *
    */
    public function orderList(Request $request) {
        $class_helper = new App\OrderController();
        $data = $class_helper->when($request,$id);
        return $data;
    }

}
