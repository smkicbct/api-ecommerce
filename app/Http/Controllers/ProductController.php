<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Product as PC;

class ProductController extends Controller
{
    /**
     * Show the Product.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = PC::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['categories'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['categories'] = 'Failed to find products!';
            return $res;
        }
     }
    
    /**
     * Show the Product.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = PC::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['categories'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['categories'] = 'Failed to find product!';
            return $res;
        }
     }

    /**
     * Store the Product.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $seller = $request->input('id_seller');
        $product = $request->input('name_product');
        $sku = $request->input('sku');
        $category = $request->input('id_category');
        $condition = $request->input('condition');
        $weight = $request->input('weight');
        $stock = $request->input('stock');
        $min = $request->input('min_buy');
        $price = $request->input('unit_price');
        $description = $request->input('description');
        
        $data = PC::create([
            'id_seller' => $seller,
            'name_product' => $product,
            'sku' => $sku,
            'id_category' => $category,
            'condition' => $condition,
            'weight' => $weight,
            'stock' => $stock,
            'min_buy' => $min,
            'unit_price' => $price,
            'description' => $description
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update product!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update product!';
            return response($res);
        }
    }

    /**
     * Update the Product.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $seller = $request->input('id_seller');
        $product = $request->input('name_product');
        $sku = $request->input('sku');
        $category = $request->input('id_category');
        $condition = $request->input('condition');
        $weight = $request->input('weight');
        $stock = $request->input('stock');
        $min = $request->input('min_buy');
        $price = $request->input('unit_price');
        $description = $request->input('description');
        
        $data = PC::findOrFail($id)->update([
            'id_seller' => $seller,
            'name_product' => $product,
            'sku' => $sku,
            'id_category' => $category,
            'condition' => $condition,
            'weight' => $weight,
            'stock' => $stock,
            'min_buy' => $min,
            'unit_price' => $price,
            'description' => $description
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update category product!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update category product!';
            return response($res);
        }
    }

}
