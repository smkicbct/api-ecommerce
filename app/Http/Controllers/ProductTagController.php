<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\ProductTag as PT;

class ProductTagController extends Controller
{
    /**
     * Show the Product Tag.
     *
     * @param $request Request
     */
     public function index(Request $request)
     {
        $data = PT::all();
        if(count($data) > 0) {
            $res['success'] = true;
            $res['tags'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['tags'] = 'Failed to find tags!';
            return $res;
        }
     }
    
    /**
     * Show the Product Tag.
     *
     * @param $request Request
     */
     public function show($id,Request $request)
     {
        $data = PT::findOrFail($id);
        if(count($data) > 0) {
            $res['success'] = true;
            $res['tags'] = $data;
            return $res;
        } else {
            $res['success'] = false;
            $res['tags'] = 'Failed to find tags!';
            return $res;
        }
     }

    /**
     * Store the Product Tag.
     *
     * @param $request Request
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $product = $request->input('id_product');
        
        $data = PC::create([
            'name' => $name,
            'id_product' => $product
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update tags!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update tags!';
            return response($res);
        }
    }

    /**
     * Update the Product Tag.
     *
     * @param $request Request
     */
    public function update($id,Request $request)
    {
        $name = $request->input('name');
        $product = $request->input('id_product');
        
        $data = PC::findOrFail($id)->update([
            'name' => $name,
            'id_product' => $product
        ]);

        if($data) {
            $res['success'] = true;
            $res['message'] = 'Success update category tags!';
            return response($res);
        } else {
            $res['success'] = false;
            $res['message'] = 'Failed to update category tags!';
            return response($res);
        }
    }

}
