<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductThumbnail extends Model
{
    protected $table = "productthumbnails";
    protected $primaryKey = "id_thumbnail";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo','id_product'
    ];

    /**
     * For relationship belongsto Product.
     *
     */
     public function product()
     {
         return $this->belongsTo('App\Product','id_product');
     }
     
}
