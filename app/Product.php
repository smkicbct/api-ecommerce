<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = "products";
    protected $primaryKey = "id_product";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_seller','name_product','sku','id_category',
        'condition','weight','stock','min_buy','unit_price','description'
    ];
    
    /**
     * For relationship.
     *
     */
     public function productCategory()
     {
         return $this->hasMany('App\Product','id_product');
     }
     
     public function productTag()
     {
        return $this->hasMany('App\ProductTag','id_tag');
     }
     
     public function productThumbnail()
     {
         return $this->hasMany('App\ProductThumbnail','id_thumbnail');
     }

}
