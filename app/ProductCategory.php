<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = "productcategories";
    protected $primaryKey = "id_category";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','id_parent'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
    ];

    /**
     * For relationship belongsto Product.
     *
     */
     public function product()
     {
         return $this->belongsTo('App\Product','id_product');
     }
     
}
