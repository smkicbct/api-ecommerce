<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $table = "regencies";
    protected $primaryKey = "id_regency";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_province','name'
    ];

    /**
     * For relationship to Districts.
     *
     * @return Relations
     */
     public function district()
     {
         return $this->hasMany('App\District','id_district');
     }
     
}
