<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'name', 'email','username','photo','id_roles','password','api_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id_user','password','api_token','remember_token','id_roles'
    ];

    /**
     * The relationship join UserDetail.
     *
     * @return belongsTo
     */
    public function userDetail()
    {
        return $this->belongsTo('App\UserDetail','id_detail');
    }
    
    /**
     * The relationship join UserRole.
     *
     * @return Relations
     */
    public function userRole()
    {
        return $this->hasOne('App\UserRole','id_roles');
    }
    
    /**
     * The relationship join UserAddress.
     *
     * @return Relations
     */
    public function userAddress()
    {
        return $this->hasMany('App\UserAddress','id_address');
    }
}
