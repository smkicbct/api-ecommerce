<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = "villages";
    protected $primaryKey = "id_village";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_district','name'
    ];

    /**
     * For relationship belongsto .
     *
     */
     
}
