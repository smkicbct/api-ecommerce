<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = "user_details";
    protected $primaryKey = "id_detail";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user','sex','dob','phone'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id_detail','id_user','id_roles'
    ];

    /**
     * For relationship belongsto UserDetail.
     *
     * @var array
     */
    public function user()
    {
        return $this->hasOne('App\User','id_user');
    }
}
