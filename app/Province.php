<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "provinces";
    protected $primaryKey = "id_province";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * For relationship to Regencies.
     *
     * @return Relations
     */
     public function regency()
     {
         return $this->hasMany('App\Regency','id_regency');
     }
     
}
