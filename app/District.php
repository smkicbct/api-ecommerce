<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = "districts";
    protected $primaryKey = "id_distric";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_regency','name'
    ];

    /**
     * For relationship to Regencies.
     *
     * @return Relations
     */
     public function village()
     {
         return $this->hasMany('App\Village','id_village');
     }
     
}
