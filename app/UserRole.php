<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = "user_roles";
    protected $primaryKey = "id_roles";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'access'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id_roles'
    ];

    /**
     * For relationship belongsto User.
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }
}
