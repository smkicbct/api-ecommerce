<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    protected $table = "producttags";
    protected $primaryKey = "id_tag";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','id_product'
    ];

    /**
     * For relationship belongsto Product.
     *
     */
     public function product()
     {
         return $this->belongsTo('App\Product','id_product');
     }
     
}
