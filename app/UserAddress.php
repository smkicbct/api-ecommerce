<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = "user_addreses";
    protected $primaryKey = "id_address";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user','name_address','on_behalf','phone',
        'id_province','id_regency','id_district','id_village','address','postal_code'
    ];
    
    /**
     * The relationship BelongsTo User.
     *
     * @return belongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User','id_user');
    }

}
