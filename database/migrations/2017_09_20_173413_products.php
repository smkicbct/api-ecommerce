<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id_product',11);
            $table->integer('id_seller')->unsigned();
            $table->foreign('id_seller')->references('id_user')->on('users');
            $table->string('name_product',255);
            $table->string('sku',255)->nullable();
            $table->integer('id_category')->unsigned();
            $table->foreign('id_category')->references('id_category')->on('productcategories');
            $table->enum('condition',['new','old']);
            $table->float('weight',8,2);
            $table->integer('stock');
            $table->integer('min_buy');
            $table->integer('unit_price');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
