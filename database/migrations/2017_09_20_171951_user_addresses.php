<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_addreses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_address',11);
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('users');
            $table->string('name_address',25);
            $table->string('on_behalf',25);
            $table->string('phone',25);
            $table->integer('id_province')->unsigned();
            $table->foreign('id_province')->references('id_province')->on('provinces');
            $table->integer('id_regency')->unsigned();
            $table->foreign('id_regency')->references('id_regency')->on('regencies');
            $table->integer('id_district')->unsigned();
            $table->foreign('id_district')->references('id_district')->on('districts');
            $table->integer('id_village')->unsigned();
            $table->foreign('id_village')->references('id_village')->on('villages');
            $table->string('address',25);
            $table->string('postal_code',25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
