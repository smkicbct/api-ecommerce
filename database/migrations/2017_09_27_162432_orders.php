<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id_order',11);
            $table->integer('id_buyer')->unsigned();
            $table->foreign('id_buyer')->references('id_user')->on('users');
            $table->integer('id_address')->unsigned();
            $table->foreign('id_address')->references('id_address')->on('user_addreses');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id_product')->on('products');
            $table->integer('quantity')->default(1);
            $table->integer('total_price')->default(1);
            $table->text('note')->nullable();
            $table->integer('payment')->unsigned();
            $table->foreign('payment')->references('id_payment')->on('payments');
            $table->integer('status')->default(1)->comment('0: Gagal, 1: Pending, 2:Kirim, 3:Terima');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
