<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class User extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // nama
        // email
        // username
        // password
        // role user
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_user',11);
            $table->string('name',90);
            $table->string('email',90)->unique();
            $table->string('username',90)->unique();
            $table->string('password',251);
            $table->string('photo',251)->nullable();
            $table->integer('id_roles')->unsigned(); // 1 : Administrator, 2: Penjual, 3: Pembeli, 0: Nonaktif.$table->foreign('id_biodata')->references('id_biodata')->on('biodatas')->nullable();
            $table->foreign('id_roles')->references('id_roles')->on('user_roles')->default(1);
            $table->string('api_token');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
