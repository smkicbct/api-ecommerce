<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*
* Middleware API Access User / Public
*
*/
$router->post('/register', 'UserController@register');
$router->post('/authenticate', 'LoginController@index');
$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->group(['prefix' => 'categories'], function () use ($router) {
        $router->get('/{id}', '');
        $router->get('/', '');
    });
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get('/info', '');
        $router->post('/info', '');
        $router->group(['prefix' => 'addresses'], function () use ($router) {
            $router->get('/','');
            $router->post('/','');
            $router->post('/','');
            $router->delete('/','');
        });
    });
    $router->group(['prefix' => 'orders'], function () use ($router) {
        $router->get('/','V1\VoneController@orderList');
        $router->post('/','');
        $router->post('/','');
        $router->delete('/','');
    });
});

/*
* Middleware API Access Administrator / Private
*
*/
$router->group(['prefix' => 'administrator','middleware' => 'auth'], function () use ($router) {
    // $router->get('/user/{id}', 'UserController@getUser'); // semuanya di detail
    $router->group(['prefix' => 'products'], function () use ($router) {
        $router->group(['prefix' => 'categories'] , function () use ($router) {
            $router->post('/', 'ProductCategoryController@store');
            $router->post('/{id}', 'ProductCategoryController@update');
            $router->get('/{id}', 'ProductCategoryController@show');
            $router->get('/', 'ProductCategoryController@index');
        });
    });
    $router->group(['prefix' => 'user_address'], function () use ($router) {
        $router->post('/', 'UserAddressController@store');
        $router->get('/', 'UserAddressController@index');
    });
    $router->group(['prefix' => 'my'], function () use ($router) {
        $router->post('detail', 'UserDetailController@action');
        $router->get('detail', 'UserDetailController@show');
    });
});
